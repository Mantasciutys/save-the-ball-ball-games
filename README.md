### Save the Ball - Ball Games ###

This file contains a brief description of the game Save the Ball - Ball Games available in both App Store and Google Play Store.

The idea of the game is simple - the player has to keep the ball on a platform at all times. Once it goes down, the game is lost. The only thing a player controls is the platform and that is done by twisting the device. However,
it is not that easy to keep that ball on a platform. Often varying winds that are getting stronger and unexpected surprises make it much harder than it seems. There are some good news too - some game helpers do appear sometimes 
that can slightly help you. But make sure you aren't discracted as the main goal is not to fall off the platform!

In case you have any questions, do not hesitate to contact us. Our email is mcgamedevelopmnt@gmail.com

Happy gaming!